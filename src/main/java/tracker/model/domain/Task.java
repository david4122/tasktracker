package tracker.model.domain;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Task {

	public static enum State {		// flyweight
		PAUSED {
			@Override
			public void switchState(Task t, State st) {
				switch(st) {
					case RUNNING:
						t.lastStarted = Instant.now();
						break;
					case CANCELLED:
					case FINISHED:
						t.duration.plus(Duration.between(t.lastStarted, Instant.now()));
						t.ended = LocalDate.now();
						break;
					default:
						throw new IllegalStateException(String.format("Cannot change state from %s to %s", t.getState(), st));
				}
				t.state = st;
			}
		}, RUNNING {
			@Override
			public void switchState(Task t, State st) {
				switch(st) {
					case FINISHED:
					case CANCELLED:
						t.ended = LocalDate.now();
					case PAUSED:
						t.duration = t.duration.plus(Duration.between(t.lastStarted, Instant.now()));
						t.lastStarted = null;
						break;
					default:
						throw new IllegalStateException(String.format("Cannot change state from %s to %s", t.getState(), st));
				}
				t.state = st;
			}
		}, FINISHED {
			@Override
			public void switchState(Task t, State st) {
				throw new IllegalStateException(String.format("Cannot change state from %s to %s", t.getState(), st));
			}
		}, CANCELLED {
			@Override
			public void switchState(Task t, State st) {
				throw new IllegalStateException(String.format("Cannot change state from %s to %s", t.getState(), st));
			}
		};

		public abstract void switchState(Task t, State st);
	}

	@Id
	@GeneratedValue
	private Long id;

	@Column
	private String name;

	@ManyToOne
	private Client client;

	@ManyToOne
	private BillingPeriod period;

	@Column
	private LocalDate started;

	@Column
	private LocalDate ended;

	@Column
	private Instant lastStarted;

	@Enumerated(EnumType.STRING)
	private State state;

	@Column
	private Duration duration;

	public Task() {}

	public Task(Client client, String name) {
		this.client = client;
		this.name = name;
		this.started = LocalDate.now();
		this.state = State.PAUSED;
		this.duration = Duration.ZERO;
	}

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BillingPeriod getBillingPeriod() {
		return period;
	}

	public void setBillingPeriod(BillingPeriod period) {
		this.period = period;
	}

	public Client getClient() {
		return client;
	}

	public LocalDate getStartedDate() {
		return started;
	}

	public LocalDate getEndedDate() {
		return ended;
	}

	public State getState() {
		return state;
	}

	public void setState(State st) {
		state.switchState(this, st);
	}

	public Duration getDuration() {
		return duration;
	}

	public Instant getLastStarted() {
		return lastStarted;
	}

	@Override
	public String toString() {
		return String.format("TASK #%d - %s [%s, %s - %s] (for %s, period %s)", id, state, started, ended, duration, client, period);
	}

	@Override
	public boolean equals(Object obj) {
		if(!(obj instanceof Task))
			return false;

		Task other = (Task) obj;
		if(null == id || null == other.id) {
			return name.equals(other.name)
				&& started.equals(other.started)
				&& state.equals(other.state)
				&& ended.equals(other.ended)
				&& lastStarted.equals(other.lastStarted)
				&& client.equals(client)
				&& period.equals(period);
		} else
			return id == other.id;
	}
}
