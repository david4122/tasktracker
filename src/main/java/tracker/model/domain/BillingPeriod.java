package tracker.model.domain;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class BillingPeriod {

	@Id
	@GeneratedValue
	private Long id;

	@Column
	private LocalDate start;

	@Column
	private LocalDate end;

	public BillingPeriod() {}

	public BillingPeriod(LocalDate start, LocalDate end) {
		this.start = start;
		this.end = end;
	}

	public Long getId() {
		return id;
	}

	public LocalDate getStart() {
		return start;
	}

	public LocalDate getEnd() {
		return end;
	}

	@Override
	public String toString() {
		return String.format("BillingPeriod: #d [%s - %s]", id, start, end);
	}
}
