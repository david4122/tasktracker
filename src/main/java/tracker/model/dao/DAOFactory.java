package tracker.model.dao;

public interface DAOFactory {

	public ClientDAO getClientDAO();

	public BillingPeriodDAO getBillingPeriodDAO();

	public TaskDAO getTaskDAO();
}
