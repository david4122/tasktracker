package tracker.model.dao;

import java.time.LocalDate;

import tracker.model.domain.BillingPeriod;

public interface BillingPeriodDAO extends DataAccessObject<BillingPeriod> {

	public BillingPeriod getForDate(LocalDate date);
}
