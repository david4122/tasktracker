package tracker.model.dao;

import java.util.List;

import tracker.model.domain.BillingPeriod;
import tracker.model.domain.Client;
import tracker.model.domain.Task;

public interface TaskDAO extends DataAccessObject<Task> {

	public List<Task> getByClient(Client c);

	public List<Task> getByBillingPeriod(BillingPeriod bp);

	public List<Task> getByState(Task.State state);
}
