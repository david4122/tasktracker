package tracker.model.dao.jpa;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import com.google.inject.Provider;
import com.google.inject.persist.Transactional;

import tracker.model.dao.DataAccessObject;

public abstract class AbstractJpaDAO<T> implements DataAccessObject<T> {

	private Provider<EntityManager> em;
	private Class<T> target;

	public AbstractJpaDAO(Class<T> target) {
		this.target = target;
	}

	@Inject
	protected void setEntityManagerProvider(Provider<EntityManager> em) {
		this.em = em;
	}

	protected EntityManager getEntityManager() {
		return em.get();
	}

	public List<T> getAll() {
		CriteriaBuilder builder = em.get().getCriteriaBuilder();
		CriteriaQuery<T> cq = builder.createQuery(target);

		return em.get().createQuery(cq.select(cq.from(target)))
			.getResultList();
	}

	public T get(Long id) {
		CriteriaBuilder cb = em.get().getCriteriaBuilder();
		CriteriaQuery<T> cq = cb.createQuery(target);
		Root<T> root = cq.from(target);
		cq.select(root).where(cb.equal(root.get("id"), id));

		return em.get().createQuery(cq).getSingleResult();
	}

	@Transactional
	public void persist(T obj) {
		em.get().persist(obj);
	}

	@Transactional
	public void update(T obj) {
		em.get().merge(obj);
	}

	@Transactional
	public void remove(Long id) {
		em.get().remove(em.get().find(target, id));
	}
}
