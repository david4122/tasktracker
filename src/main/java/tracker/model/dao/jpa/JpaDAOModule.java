package tracker.model.dao.jpa;

import com.google.inject.AbstractModule;
import com.google.inject.persist.jpa.JpaPersistModule;

import tracker.model.dao.BillingPeriodDAO;
import tracker.model.dao.ClientDAO;
import tracker.model.dao.TaskDAO;

public class JpaDAOModule extends AbstractModule {

	@Override
	protected void configure() {
		install(new JpaPersistModule("tasktracker"));

		bind(ClientDAO.class).to(ClientJpaDAO.class);
		bind(BillingPeriodDAO.class).to(BillingPeriodJpaDAO.class);
		bind(TaskDAO.class).to(TaskJpaDAO.class);
	}
}
