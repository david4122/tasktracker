package tracker.model.dao.jpa;

import tracker.model.dao.ClientDAO;
import tracker.model.domain.Client;

public class ClientJpaDAO extends AbstractJpaDAO<Client> implements ClientDAO {

	public ClientJpaDAO() {
		super(Client.class);
	}
}
