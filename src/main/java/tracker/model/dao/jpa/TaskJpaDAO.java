package tracker.model.dao.jpa;

import java.util.List;

import javax.persistence.TypedQuery;

import tracker.model.dao.TaskDAO;
import tracker.model.domain.BillingPeriod;
import tracker.model.domain.Client;
import tracker.model.domain.Task;
import tracker.model.domain.Task.State;

public class TaskJpaDAO extends AbstractJpaDAO<Task> implements TaskDAO {

	public TaskJpaDAO() {
		super(Task.class);
	}

	@Override
	public List<Task> getByClient(Client c) {
		TypedQuery<Task> q = getEntityManager().createQuery("SELECT t FROM Task t WHERE t.client = :client",
				Task.class);
		q.setParameter("client", c);

		return q.getResultList();
	}

	@Override
	public List<Task> getByBillingPeriod(BillingPeriod bp) {
		TypedQuery<Task> q = getEntityManager().createQuery("SELECT t FROM Task t WHERE t.period = :bperiod",
				Task.class);
		q.setParameter("bperiod", bp);

		return q.getResultList();
	}

	@Override
	public List<Task> getByState(State state) {
		TypedQuery<Task> q = getEntityManager().createQuery("SELECT t FROM Task t WHERE t.state = :state",
				Task.class);
		q.setParameter("state", state);

		return q.getResultList();
	}
}
