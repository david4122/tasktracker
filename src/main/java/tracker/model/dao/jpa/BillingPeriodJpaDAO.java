package tracker.model.dao.jpa;

import java.time.LocalDate;

import javax.persistence.TypedQuery;

import tracker.model.dao.BillingPeriodDAO;
import tracker.model.domain.BillingPeriod;

public class BillingPeriodJpaDAO extends AbstractJpaDAO<BillingPeriod> implements BillingPeriodDAO {

	public BillingPeriodJpaDAO() {
		super(BillingPeriod.class);
	}

	@Override
	public BillingPeriod getForDate(LocalDate date) {
		TypedQuery<BillingPeriod> q = getEntityManager().createQuery(
				"SELECT bp FROM BillingPeriod bp WHERE :date BETWEEN bp.start AND bp.end",
				BillingPeriod.class);
		q.setParameter("date", date);

		return q.getSingleResult();
	}
}
