package tracker.model.dao;

import tracker.model.domain.Client;

public interface ClientDAO extends DataAccessObject<Client> {}
