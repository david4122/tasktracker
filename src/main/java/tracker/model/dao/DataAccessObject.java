package tracker.model.dao;

import java.util.List;

public interface DataAccessObject<T> {

	public List<T> getAll();

	public T get(Long id);

	public void persist(T obj);

	public void update(T obj);

	public void remove(Long id);
}
