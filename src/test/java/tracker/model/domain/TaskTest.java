package tracker.model.domain;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static tracker.model.domain.Task.State.CANCELLED;
import static tracker.model.domain.Task.State.FINISHED;
import static tracker.model.domain.Task.State.PAUSED;
import static tracker.model.domain.Task.State.RUNNING;

import java.util.concurrent.TimeUnit;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

public class TaskTest {

	@Test
	public void testStates() throws Exception {
		Client cl = new Client("Test client");
		Task t = new Task(cl, "TEST TASK PERSIST");

		assertEquals(PAUSED, t.getState());

		t.setState(RUNNING);
		t.setState(PAUSED);
		assertThrows(IllegalStateException.class, () -> t.setState(PAUSED));
		t.setState(RUNNING);
		assertThrows(IllegalStateException.class, () -> t.setState(RUNNING));
		t.setState(FINISHED);

		assertThrows(IllegalStateException.class, () -> t.setState(PAUSED));
		assertThrows(IllegalStateException.class, () -> t.setState(RUNNING));
		assertThrows(IllegalStateException.class, () -> t.setState(CANCELLED));
		assertThrows(IllegalStateException.class, () -> t.setState(FINISHED));
	}

	@Test
	@Tag("slow")
	public void testDuration() throws Exception {
		Client cl = new Client("Test client");
		Task t = new Task(cl, "TEST TASK PERSIST");

		t.setState(RUNNING);
		TimeUnit.SECONDS.sleep(5);
		t.setState(FINISHED);

		assertEquals(5, t.getDuration().toSeconds());
	}
}
