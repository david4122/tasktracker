package tracker.model.dao.jpa;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.persist.PersistService;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import tracker.model.dao.ClientDAO;
import tracker.model.dao.TaskDAO;
import tracker.model.domain.Client;
import tracker.model.domain.Task;

public class TaskJpaTest {

	private static Injector injector;

	@BeforeAll
	public static void setup() {
		injector = Guice.createInjector(new JpaDAOModule());
		injector.getInstance(PersistService.class).start();
	}

	@Test
	public void testPersist() throws Exception {
		Client cl = new Client("TEST TASK PERSIST");
		Task t = new Task(cl, "TEST TASK PERSIST");

		injector.getInstance(ClientDAO.class).persist(cl);
		injector.getInstance(TaskDAO.class).persist(t);

		assertNotNull(t.getId());

		Task res = injector.getInstance(TaskDAO.class).get(t.getId());

		assertEquals(t, res);
	}

	@Test
	public void testRemove() throws Exception {
		Client cl = new Client("TEST TASK REMOVE");
		Task t = new Task(cl, "TEST TASK PERSIST");

		injector.getInstance(ClientDAO.class).persist(cl);
		injector.getInstance(TaskDAO.class).persist(t);

		assertTrue(injector.getInstance(TaskDAO.class).getAll().contains(t));

		injector.getInstance(TaskDAO.class).remove(t.getId());

		assertFalse(injector.getInstance(TaskDAO.class).getAll().contains(t));
	}

	@Test
	public void testUpdate() throws Exception {
		Client cl = new Client("TEST TASK UPDATE");
		Task t = new Task(cl, "TEST TASK PERSIST");

		injector.getInstance(ClientDAO.class).persist(cl);
		injector.getInstance(TaskDAO.class).persist(t);

		t.setName("TEST TASK UPDATE");

		injector.getInstance(TaskDAO.class).update(t);

		assertEquals(t.getName(), injector.getInstance(TaskDAO.class).get(t.getId()).getName());
	}

	@Test
	public void testConcurrent() throws Exception {
		ExecutorService exe = Executors.newFixedThreadPool(10);
		TaskDAO dao = injector.getInstance(TaskDAO.class);
		Client cl = new Client("TEST TASK CONCURRENT");
		for(int i = 0; i<10; i++) {
			exe.execute(() -> {
				dao.persist(new Task(cl, "TEST TASK PERSIST"));
			});
		}
	}
}
